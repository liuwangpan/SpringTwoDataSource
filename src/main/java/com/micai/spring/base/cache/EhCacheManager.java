/**
 * 
 */
package com.micai.spring.base.cache;

import java.io.Serializable;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 缓存管理类
 * 
 * @author ZhaoXinGuo
 * 
 */
public class EhCacheManager {

	private static Logger logger = LoggerFactory
			.getLogger(EhCacheManager.class);

	private static final String CACHE_KEY = "serviceCache";
	
	public static final int CACHE_LIVE_SECONDS = 180;

	private static EhCacheManager instance = new EhCacheManager();

	private static CacheManager cacheManager;

	private static Ehcache fileCache;

	private EhCacheManager() {
		logger.info("初始化缓存 ----------------------------------------");
		cacheManager = new CacheManager();
		fileCache = cacheManager.getCache(CACHE_KEY);
		logger.info("初始化缓存成功....");
	}

	public static synchronized EhCacheManager getInstance() {
		if (instance == null) {
			instance = new EhCacheManager();
		}
		return instance;
	}

	public static byte[] loadFile(String key) {
		Element element = fileCache.get(key);
		if (element != null) {
			Serializable serializable = element.getValue();
			if (serializable != null) {
				return (byte[]) serializable;
			}
		}
		return null;
	}

	public static void cacheFile(String key, byte[] value) {
		fileCache.put(new Element(key, value));
	}

	/**
	 * 将数据存入缓存，缓存无时间限制
	 * 
	 * @param key
	 * @param value
	 */
	public static <T> void put(String key, T value) {
		fileCache.put(new Element(key, value));
	}
	
	/**
	 * 通过key值获取存入缓存中的数据
	 * 
	 * @param key
	 * @return
	 */
	public static <T> T get(String key){
		Element element = fileCache.get(key);
		if(element == null){
			if(logger.isDebugEnabled()){
				logger.debug("not found key:" + key);
			}
			return null;
		}
		T t = (T)element.getObjectValue();
		return t;
	}
	
	/**
	 * 根据key删除缓存
	 * 
	 * @param key
	 * @return
	 */
	public static boolean remove(String key){
		logger.info("remove key:" +key);
		return fileCache.remove(key);
	}
	
	/**
	 * 关闭cacheManager对象
	 */
	public static void shutdown(){
		cacheManager.shutdown();
	}
	
}