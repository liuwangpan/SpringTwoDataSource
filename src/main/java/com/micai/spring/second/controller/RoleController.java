/**
 * 
 */
package com.micai.spring.second.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.micai.spring.second.po.Role;
import com.micai.spring.second.service.RoleService;

/**
 * @author ZhaoXinGuo
 *
 */
@Controller
public class RoleController{

	@Autowired
	private RoleService roleService;
	
	public void save(Role role){
		roleService.saveRole(role);
	}
}
