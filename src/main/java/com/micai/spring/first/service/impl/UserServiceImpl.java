/**
 * 
 */
package com.micai.spring.first.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.micai.spring.first.dao.UserDao;
import com.micai.spring.first.po.User;
import com.micai.spring.first.service.UserService;

/**
 * @author ZhaoXinGuo
 *
 * @email sxdtzhaoxinguo@163.com
 */
@Service("userService")
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao userDao;

	public void saveUser(User user) {
		userDao.saveUser(user);
	}

	public List<User> list() {
		return userDao.list();
	}
	
	
}
